package Day3;

import java.util.Scanner;

public class Modified{
	static String modifiedString(String string, char character1, char character2) {
		String newString = "";
		if (string.charAt(0) == character1) {
			newString += string.charAt(0);
		} else if (string.charAt(1) == character2) {
			newString += string.charAt(1);
		}
		for (int i = 2; i < string.length(); i++) {

			newString += string.charAt(i);

		}

		return newString;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the String");
		String string = scanner.next();
		System.out.println("Enter the first char");
		char character1 = scanner.next().charAt(0);
		System.out.println("Enter the second char");
		char character2 = scanner.next().charAt(0);
		System.out.println(modifiedString(string, character1, character2));

		scanner.close();
	}

}
