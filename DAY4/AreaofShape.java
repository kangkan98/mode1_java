package Day4; 

import java.text.DecimalFormat;
import java.util.*;


abstract class Shape {
	protected String name;

	public Shape(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public abstract float calculateArea() ;



}



class Square extends Shape{
	private int side;
	public Square(String name,int side) {
		super(name);
		this.side = side;
		
	}

	@Override
	public float calculateArea() {
		
		return getSide()*getSide();
	}

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}

}


class Rectangle extends Shape{
	private int length;
	private int breadth;


	public Rectangle(String name, int length, int breadth) {
		super(name);
		this.length = length;
		this.breadth = breadth;
		
	}

	@Override
	public float calculateArea() {
		
		return getBreadth()*getLength();
	}
	
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getBreadth() {
		return breadth;
	}

	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}
}


class Circle extends Shape{
	private int radius;
	final float pi =  (float) 3.1415;

	public Circle(String name, int radius) {
		super(name);
		this.radius = radius;
	}


	@Override
	public float calculateArea() {
		
		
		return (float) (pi*(getRadius()*getRadius()));
	}


	public int getRadius() {
		return radius;
	}


	public void setRadius(int radius) {
		this.radius = radius;
	}

}

class AreaofShape {

	public static void main(String[] args) {
		DecimalFormat df2 = new DecimalFormat("#.##");
		Scanner scanner = new Scanner(System.in);
		System.out.println("shapes : ");
		scanner.next();
		scanner.next();
		scanner.next();
		
		System.out.println("Shape name : ");
		String name = scanner.next();
		
		if(name.equals("Circle")) {
			System.out.println("radius : ");
			int radius = scanner.nextInt();
			Circle circle = new Circle(name, radius);
			System.out.println("Area of the circle is : "+ df2.format(circle.calculateArea()));
		}
		else if(name.equalsIgnoreCase("Square")) {
			System.out.println("side : ");
			int side = scanner.nextInt();
			Square square = new Square(name, side);
			System.out.println("Area of the Square is : "+df2.format( square.calculateArea()));
		}
		else if(name.equalsIgnoreCase("Rectangle")) {
			System.out.println("length : ");
			int length = scanner.nextInt();
			System.out.println("Breadth : ");
			int breadth = scanner.nextInt();
			Rectangle rectangle = new Rectangle(name, length, breadth);
			System.out.println("Area of the Rectangle is : "+ df2.format(rectangle.calculateArea()));
		}
		
		scanner.close();
	}
	
}
