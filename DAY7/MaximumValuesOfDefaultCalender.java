package day7;

import java.util.Calendar;

public class MaximumValuesOfDefaultCalender {

	/**
	 * @param args
	 */
	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		System.out.println("Time : "+calendar.getTime());
		System.out.println("Actual maximum Year : "+calendar.getActualMaximum(calendar.YEAR));
		System.out.println("Actual maximum Month : "+calendar.getActualMaximum(calendar.MONTH));
		System.out.println("Actual maximum Date : "+calendar.getActualMaximum(calendar.DATE));
		System.out.println("Actual maximum Week of the year: "+calendar.getActualMaximum(calendar.WEEK_OF_YEAR));
	}

}
