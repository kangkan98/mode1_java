package Day3;

import java.util.Scanner;

public class Search {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array = new int[10];
		boolean checkStatus = false;
		System.out.println("Enter the intergers into Array");
		Scanner scanner = new Scanner(System.in);

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		
		System.out.println("Enter the element to Search");
		int number = scanner.nextInt();
		System.out.println("Elements in the Array:");
		for (int i = 0; i < array.length - 1; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println(array[array.length - 1]);

		for (int i = 0; i < array.length; i++) {
			if (array[i] == number) {
				checkStatus = true;
			}
		}
		System.out.println("------------------------------------");
		if (checkStatus) {
			System.out.println(number + " Element was found");
		} else {
			System.out.println(number + " Element was not found");
		}

		scanner.close();
	}

}