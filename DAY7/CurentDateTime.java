package day7;



import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class CurentDateTime {

	public static void main(String[] args) {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime localDateTime = LocalDateTime.now();
		System.out.println(dateTimeFormatter.format(localDateTime));

	}

}
