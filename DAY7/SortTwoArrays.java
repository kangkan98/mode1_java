package day7;

import java.util.Scanner;


public class SortTwoArrays {

	/**
	 * @param args
	 */
	
	static int[] sortMergedArray(int[] array1,int[] array2) {
		int[] sortedArray = new int[3];
		int arr1len = array1.length;
		int arr2len = array2.length;
		int[] result = new int[arr1len+arr2len];
		System.arraycopy(array1, 0, result, 0,arr1len);
		System.arraycopy(array2, 0, result, arr1len, arr2len);
		for(int i = 0;i<result.length;i++) {
			for(int j=i+1;j<result.length;j++) {
				if(result[i]>result[j]) {
					int temp = result[i];
					result[i] = result[j];
					result[j] = temp;				}
			}
		}
		sortedArray[0] = result[2];
		sortedArray[1] = result[6];
		sortedArray[2] = result[8];
		return sortedArray;
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int[] array1 = new int[5];
		int[] array2 = new int[5];
		System.out.println("enter the array1 elements : ");
		for(int i=0;i<5;i++) {
			array1[i]=scanner.nextInt();
		}
		System.out.println("enter the array2 elements : ");
		for(int i=0;i<5;i++) {
			array2[i]=scanner.nextInt();
		}
		int[] result = new int[3];
		result = sortMergedArray(array1, array2);
		for (int i=0;i<result.length;i++) {
			System.out.println(result[i]);
		}
		scanner.close();
	}

}