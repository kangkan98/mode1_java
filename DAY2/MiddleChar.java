package day2;

import java.util.Scanner;

public class MiddleChar {
	public static void main(String [] args) {
		Scanner var = new Scanner(System.in);
		String s = var.nextLine(); 
		int l = s.length();
		if (l%2!=0) {
			System.out.println(s.charAt(l/2));
		}
		
		else {
			System.out.print(s.charAt(l/2));
			System.out.print(s.charAt((l/2)-1));
		}
	}

}
