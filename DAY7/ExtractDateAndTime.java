package day7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class ExtractDateAndTime {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String dateAndTime = scanner.nextLine();
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(dateAndTime);
			System.out.println(dateAndTime+" is : "+date);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		scanner.close();
	}

}
