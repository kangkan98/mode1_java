package day7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class DateFormat {

	/**
	 * @param args
	 * @throws ParseException 
	 */
	static String convertDateFormate(String date) throws ParseException {
		
		@SuppressWarnings("deprecation")
		long date2 = Date.parse(date);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yy");
		String date1 = simpleDateFormat.format(date2);
		return date1;
	}
	public static void main(String[] args) throws ParseException {
		Scanner scanner = new Scanner(System.in);
		String date = scanner.next();
		System.out.println(convertDateFormate(date));
		scanner.close();
	}

}