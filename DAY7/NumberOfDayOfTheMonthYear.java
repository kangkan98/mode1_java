package day7;


import java.time.YearMonth;
import java.util.Scanner;


public class NumberOfDayOfTheMonthYear {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int month = scanner.nextInt();
		int year = scanner.nextInt();
		YearMonth yearMonth = YearMonth.of(year, month);
		System.out.println("Number of Days In Month of the Year : "+ yearMonth.lengthOfMonth());
		scanner.close();
	}

}
