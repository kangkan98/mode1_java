package day7;




import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

 class SortByDesignation{
	

	public static ArrayList<String> obtainDesignation(HashMap<String, String> map, String designation) {
		ArrayList<String> names = new ArrayList<>();
		
		
		for(Entry<String, String> entry : map.entrySet()) {
			if(designation.equals(entry.getValue())) {
				names.add(entry.getKey());
			}
		}
		return names;
		
		//return names;
	}
}



public class SortByDesignationMain {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
	HashMap<String, String> map = new HashMap<>();
	System.out.println("Enter the no of entries : ");
	int count = scanner.nextInt();
	System.out.println("Enter the designation to filter :");
	String designation = scanner.next();
	System.out.println("Enter the Name And Designation");
	for(int i =0;i<count;i++) {
		String name =scanner.next();
		String desig = scanner.next();
		map.put(name,desig);
	}
	
	ArrayList<String> names = SortByDesignation.obtainDesignation(map,designation);
	System.out.println("Employees with the designation "+designation);
	for (String string : names) {
		System.out.println(string);
	}
	scanner.close();
	}
}