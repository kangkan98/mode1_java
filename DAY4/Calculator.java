package Day4;
import java.util.*;



interface AdvancedArthimetic {
	int divisorSum (int number);
}

class Mycalculator implements AdvancedArthimetic {

	@Override
	public int divisorSum(int number) {
		System.out.println("I implemented : AdvancedArthimetic");
		int sum = 0;
		for (int i = 1; i <= number / 2; i++) {
			if (number % i == 0) {
				sum += i;
			}
		}
		return sum+number;
	}

}

class Calculator {

	
	public static void main(String[] args) {
		AdvancedArthimetic mycalculator = new Mycalculator();
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number : ");
		int number = scanner.nextInt();
		System.out.println("The Divisor Sum of the Number "+number+" is : "+ mycalculator.divisorSum(number));
		
		scanner.close();
		
	}

}