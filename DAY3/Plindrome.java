package Day3;



import java.util.Scanner;

public class Plindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		String string = scanner.nextLine().toLowerCase();
		String reverseString = "";
		for(int i=string.length()-1;i>=0;i--) {
			reverseString+=string.charAt(i);
		}
		
		if(string.equals(reverseString)) {
			System.out.println("Given String is Palindrom");
		}
		else {
			System.out.println("Given String is Not Palindrom");
		}
		scanner.close();
	}

}