package Day5;
import java.util.*;


class CustomException extends Exception{

	/**
	 * 
	 */
	 static final long serialVersionUID = 1L;
	
	public CustomException(String s) {
		super(s);
	}

	public char[] getMessage() {
		// TODO Auto-generated method stub
		return null;
	}
}


 class IPLPlayerAge {


	@SuppressWarnings("resource")
	public static void main(String[] args) throws CustomException{
		Scanner scanner = new Scanner(System.in);
		
		try {
		System.out.println("Enter The Player Name :" );
		String playerName = scanner.nextLine();
		System.out.println("Enter The Player Age :" );
		int playerAge = scanner.nextInt();
		if(playerAge< 19) {
			throw new CustomException("InvalidAgeRange");
		}
		
		System.out.println("Enter The Player Name : " + playerName);
		System.out.println("Enter The Player Age : " +playerAge);
		}
		catch(CustomException ce) {
			System.err.println(ce.getMessage());
		}
		scanner.close();
		
		

	}

}
