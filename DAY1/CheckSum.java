package Q1;

import java.util.Scanner;
class Pack
{
    static int checksum(int k)
    {
        int sum=0,r;
        while(k>0)
        {
            r=k%10;
            sum=sum+r;
            k=k/10;
        }
        if(sum%2==0)
        {
            return -1;
            
        }
        else
        {
            return 1;
        }
    }
}
class CheckSum
{
    public static void main(String args[])
    {
        Scanner p=new Scanner(System.in);
        int j=p.nextInt();
        System.out.println(Pack.checksum(j));
    }
}