package Day3;

import java.util.Scanner;

public class Sorting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array = new int[10];
		System.out.println("Enter the intergers into Array");
		Scanner scanner = new Scanner(System.in);
		int temp;
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
	
		System.out.println("Before Sorting the Array:");
		for (int i = 0; i < array.length - 1; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println(array[array.length - 1]);

		for (int i = 0; i < array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[i] > array[j]) {
					temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}
	
		System.out.println("After Sorting the Array:");
		for (int i = 0; i < array.length - 1; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println(array[array.length - 1]);

		scanner.close();
	}

}
