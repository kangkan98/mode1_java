package com.casestudy.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.Pet;



@Component
public class PetValidator implements Validator {

	public boolean supports(Class<?> clazz) {

		return Pet.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "petName", "pet.petName.empty");
		ValidationUtils.rejectIfEmpty(errors, "petAge", "pet.petAge.empty");
		ValidationUtils.rejectIfEmpty(errors, "petPlace", "pet.petPlace.empty");
		
		
		Pet pet = (Pet) target;
		int petAge=pet.getPetAge();
		if(!(petAge>=0 && petAge<=99))
			{
			errors.rejectValue("petAge", "pet.petAge.agerange");
				
			}
		
	}

}
