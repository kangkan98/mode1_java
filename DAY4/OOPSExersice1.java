package Day4;

class FirstClass {
	int a = 100;

	public FirstClass() {
		System.out.println("--------in the constructor of class FirstClass: ");
		System.out.println("a = " + a);
		a = 333;
		System.out.println("a = " + a);
	}

	public void setFirstClass(int value) {
		a = value;
	}

	public int getFirstClass() {
		return a;
	}


}

class SecondClass {
	double b = 123.45;

	public SecondClass() {
		System.out.println("-----in the constructor of SecondClass: ");
		System.out.println("b = " + b);
		b = 3.14159;
		System.out.println("b = " + b);
	}

	public void setSecondClass(double value) {
		b = value;
	}

	public double getSecondClass() {
		return b;
	}

}


class OOPSExersice1 {


	public static void main(String[] args) {
	      FirstClass firstClass = new FirstClass(); 
	      SecondClass secondClass = new SecondClass(); 
	      System.out.println("----------------in main(): "); 
	      System.out.println("objA.a = "+firstClass.getFirstClass()); 
	      System.out.println("objB.b = "+secondClass.getSecondClass()); 
	      System.out.println("-----------------After the Updation ");
	      firstClass.setFirstClass (222); 
	      secondClass.setSecondClass (333.33); 
	      System.out.println("objA.a = "+firstClass.getFirstClass()); 
	      System.out.println("objB.b = "+secondClass.getSecondClass()); 
	    } 



}
