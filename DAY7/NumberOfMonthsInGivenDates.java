package day7;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class NumberOfMonthsInGivenDates {

	/**
	 * @param args
	 */
	static int getMonthDifference(String fromDate, String toDate) {
		int differ = Math.abs((int) ChronoUnit.MONTHS.between(LocalDate.parse(fromDate).withDayOfMonth(1), LocalDate.parse(toDate).withDayOfMonth(1)));
		return differ;
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String fromDate = scanner.next();
		String toDate = scanner.next();
		System.out.println(getMonthDifference(fromDate, toDate));
		scanner.close();
	}

}