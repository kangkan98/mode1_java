package day7;

import java.util.Scanner;

public class FirstLastCharCheck {

	/**
	 * @param args
	 */
	static int checkCharacter(String sentence) {
		sentence = sentence.toLowerCase();
		if(sentence.charAt(0) == sentence.charAt(sentence.length()-1)) {
			return 1;
		}
		else {
			return -1;
		}
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String sentence = scanner.nextLine();
		int check = FirstLastCharCheck.checkCharacter(sentence);
		if(check == 1) {
			System.out.println("Vaild");
		}
		else {
			System.out.println("InVaild");
		}
		scanner.close();
	}

}
