package Day3;

import java.util.Scanner;

public class Substring {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the String :");
		String string = scanner.nextLine();
		System.out.println("Enter the Starting index :");
		int startIndex = scanner.nextInt();
		System.out.println("Enter the Ending index :");
		int endIndex = scanner.nextInt();
		System.out.println("SubString :");
		System.out.println(string.substring(startIndex, endIndex));
		
		scanner.close();
	}

}
