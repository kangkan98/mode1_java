package day7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class OlderDate {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String s1 = sc.nextLine();
		String s2 = sc.nextLine();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");
		Date d1 = sdf.parse(s1);
		Date d2 = sdf.parse(s2);
		Calendar cal = Calendar.getInstance();
		cal.setTime(d1);
		long y = cal.getTimeInMillis();
		cal.setTime(d2);
		long y1 = cal.getTimeInMillis();
		String s3 = sdf1.format(d1);
		String s4 = sdf1.format(d2);
		if (y < y1)
			System.out.println(s3);
		else
			System.out.println(s4);
		sc.close();
	}

}
