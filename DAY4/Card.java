package Day4;
import java.util.*;
class Card1 {
	protected String holderName;
	protected String cardNumber;
	protected String expiryDate;
	public Card1(String holderName, String cardNumber, String expiryDate) {
		super();
		this.holderName = holderName;
		this.cardNumber = cardNumber;
		this.expiryDate = expiryDate;
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
}

class MembershipCard extends Card1{
	private int rating;

	public MembershipCard(String holderName, String cardNumber, String expiryDate, int rating) {
		super(holderName, cardNumber, expiryDate);
		this.rating = rating;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
}

class PaybackCard extends Card1{
	private int pointsEarned;
	private double totalAmount;

	public PaybackCard(String holderName, String cardNumber, String expiryDate, int pointsEarned, double totalAmount) {
		super(holderName, cardNumber, expiryDate);
		this.pointsEarned = pointsEarned;
		this.totalAmount = totalAmount;
	}

	public int getPointsEarned() {
		return pointsEarned;
	}

	public void setPointsEarned(int pointsEarned) {
		this.pointsEarned = pointsEarned;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}


}

class Card {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Select the Card :");
		System.out.println("1.Membership Card");
		System.out.println("2.Payback Card :");
		System.out.println("Select the option");
		int input = scanner.nextInt();
		System.out.println("Enter the Card Details : ");
		String details = scanner.next();
		String[] words = details.split("[|]");
		switch(input) {
		case 1:
			System.out.println("Enter the Rating");
			int rating = scanner.nextInt();
			MembershipCard card1 = new MembershipCard(words[0], words[1], words[2], rating);
			System.out.println(card1.getHolderName()+"'s Mambership Card Details");
			System.out.println("Card Number : "+ card1.getCardNumber());
			System.out.println("Expiry Date : "+ card1.getExpiryDate());
			System.out.println("Rating : "+ card1.getRating());
			break;
		case 2:
			System.out.println("Enter Points in the Card");
			int points = scanner.nextInt();
			System.out.println("Enter the Amount");
			double amount = scanner.nextDouble();
			PaybackCard card2 = new PaybackCard(words[0], words[1], words[2], points, amount);
			System.out.println(card2.getHolderName()+"'s Payback Card Details");
			System.out.println("Card Number : "+ card2.getCardNumber());
			System.out.println("Expiry Date : "+ card2.getExpiryDate());
			System.out.println("Points Earned : "+ card2.getPointsEarned());
			System.out.println("Total Amount : "+ card2.getTotalAmount());
			break;
		default:
			System.out.println("Select the Vaild Card");
			break;
		}
		scanner.close();
	}

}