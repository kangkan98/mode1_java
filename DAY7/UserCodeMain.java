package day7;

import java.time.YearMonth;
import java.util.Scanner;


public class UserCodeMain {

	/**
	 * @param args
	 */
	static int getNumberOfDays(int month, int year) {
		YearMonth yearMonth = YearMonth.of(year, month+1);
		return yearMonth.lengthOfMonth();
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the month code: ");
		int month = scanner.nextInt();
		System.out.println("Enter the year: ");
		int year = scanner.nextInt();
		System.out.println("The number of Days is: "+UserCodeMain.getNumberOfDays(month, year));
		scanner.close();

	}

}
