package day7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;


public class DaysDifferent {

	/**
	 * @param args
	 * @throws ParseException 
	 */
	static int daysDifference(String date1, String date2) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = simpleDateFormat.parse(date1);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		long date3 = calendar.getTimeInMillis();
		date = simpleDateFormat.parse(date2);
		calendar.setTime(date);
		long date4 = calendar.getTimeInMillis();
		int days = Math.abs((int)((date3 - date4)/(1000*3600*24)));
		return days;
	}
	public static void main(String[] args) throws ParseException {
		Scanner scanner = new Scanner(System.in);
		String date1 =scanner.next();
		String date2 =scanner.next();
		System.out.println(daysDifference(date1, date2));
		scanner.close();
	}

}
