package day7;

import java.util.Scanner;


public class IPValidOrNot {

	/**
	 * @param args
	 */
	static int IPValidator(String ip) {
		String[] stringArray = ip.split("[.]");
		boolean checkstatus = true;
		for(int i=0;i<stringArray.length;i++) {
			if((Integer.parseInt(stringArray[i])<0 || Integer.parseInt(stringArray[i])>255)) {
				checkstatus = false;
			}
		}
		if(checkstatus==true) {
			return 1;
		}
		else {
			return -1;
		}
	}
	public static void main(String[] args) {
		Scanner scanner =new Scanner(System.in);
		String IPAddress = scanner.nextLine();
		int result = IPValidator(IPAddress);
		if(result ==1) {
			System.out.println("Valid");
		}
		else {
			System.out.println("Invalid");
		}
				
		scanner.close();
	}

}
