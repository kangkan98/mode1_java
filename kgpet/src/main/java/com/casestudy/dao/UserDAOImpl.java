package com.casestudy.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Repository
public class UserDAOImpl implements UserDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public User saveUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(user);
		return user;
	}

	public User authenticateUser(String userName, String password) {

		User user = null;
		Session session = this.sessionFactory.getCurrentSession();
		user = (User) session.createQuery("FROM User U WHERE U.userName= ? AND U.userPassword= ?")
				.setParameter(0, userName).setParameter(1, password).uniqueResult();
		if (user == null) {
			user = (User) session.createQuery("FROM User U WHERE U.userName= :user_name ")
					.setParameter("user_name", userName).uniqueResult();
		}
		return user;
	}

	public Pet savePet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(pet);
		return pet;
	}

	public List<Pet> getAllPets() {

		Session session = this.sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<Pet> pets = (List<Pet>) session.createQuery(" FROM Pet").list();

		return pets;
	}

	public List<Pet> getMyPets(int userId) {

		Session session = this.sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<Pet> pets = (List<Pet>) session.createQuery(" FROM Pet P WHERE userId= :user_id ")
				.setParameter("user_id", userId).list();

		return pets;
	}

	public Pet buyPet(int userId, int petId) {
		Session session = this.sessionFactory.getCurrentSession();
		Pet pet = null;

		System.out.println(userId + petId);
		String s = Integer.toString(userId);
		String i = Integer.toString(petId);

		String str = "UPDATE pet SET USERID= :user_id WHERE PETID= :pet_id";
		session.createNativeQuery(str).setParameter("user_id", s).setParameter("pet_id", i).executeUpdate();

		return pet;

	}

}
