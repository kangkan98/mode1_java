package Day3;

import java.util.Scanner;

 class Cal {
	public int add(int num1, int num2) {
		return num1 + num2;
	}

}


public class Calculator{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cal calculator = new Cal();
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the First Number");
		int number1 = scanner.nextInt();
		System.out.println("Enter the Second Number");
		int number2 = scanner.nextInt();

		System.out.println(
				"Addition of Numbers : " + number1 + " + " + number2 + " = " + calculator.add(number1, number2));
		
	}
	

}