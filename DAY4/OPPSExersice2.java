package Day4;

class OPPSExersice2 {

	public static void main(String[] args) {
		ClassA objA = new ClassA(); 
        ClassB objB1 = new ClassB(); 
        ClassA objB2 = new ClassB(); 
        ClassC objC1 = new ClassC(); 
        ClassB objC2 = new ClassC(); 
        ClassA objC3 = new ClassC(); 
        objA.display(); 
        objB1.display(); 
        objB2.display(); 
        objC1.display(); 
        objC2.display(); 
        objC3.display();    


	}

}

class ClassA {
	int a = 100; 
    public void display() { 
        System.out.printf("a in A = %d\n", a);
    }
}

class ClassB extends ClassA {
	private int a = 123;

	public void display() {
		System.out.printf("a in B = %d\n", a);
	}

}

class ClassC extends ClassB{
	private int a = 543; 
    public void display() { 
        System.out.printf("a in C = %d\n", a); 
    } 

}


