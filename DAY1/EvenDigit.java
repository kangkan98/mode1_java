package Q1;

import java.util.Scanner;
class Usermaincode
{
    static int sumofsquareofevendigit(int k)
    {
        int sum=0,r;
        while(k>0)
        {
            r=k%10;
            if(r%2==0)
            {
                sum=sum+(r*r);
                
            }
            k=k/10;
        }
        return sum;
    }
}
class EvenDigit
{
    public static void main(String args[])
    {
        Scanner p=new Scanner(System.in);
        int j=p.nextInt();
        System.out.println(Usermaincode.sumofsquareofevendigit(j));
    }
}