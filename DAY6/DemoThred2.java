package DAY6;



 class DemoThread2 extends Thread{
	public void run() {
		try {
			System.out.println("Running child thread in loop : ");
			for(int i=0 ; i<10; i++) {
				Thread.sleep(2000);
				System.out.println(i);
			}
		}
			catch (InterruptedException ie) {
				System.err.println(ie.getMessage());
			}
		}
	public DemoThread2() {
		
		Thread thread2 = new Thread();
		thread2.start();
	}
}



 class DemoThred2 {


	public static void main(String[] args) {
		DemoThread2 demoThread1 = new DemoThread2();
		demoThread1.start();
		DemoThread2 demoThread2 = new DemoThread2();
		demoThread2.start();
		DemoThread2 demoThread3 = new DemoThread2();
		demoThread3.start();
		
	}
}